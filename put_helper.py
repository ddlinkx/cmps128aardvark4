#!flask/bin/python
# CMPS128 - cmps129aardvark4
# {chhsiao, autbrown, tebding}@ucsc.edu
# PUT Helper Functions

import re
from vc_helper import vc_get_timestamp, vc_get_empty_vc
from env_helper import env_get_rep_ipport_list
from random import randrange


# Used to randomly select a replica to forward to.
def put_get_forward_ipport(view):
    '''
    :param rep_list: list of replicas a proxy thinks is alive
    :return: the ipport index of a single randomly selected replica
    '''
    rep_list = env_get_rep_ipport_list(view)
    return rep_list[randrange(0, len(rep_list))]

# Use to generate an empty key value entry
def put_get_empty_kv(part, pid):
    '''
    :param key: key
    :return: empty key value
    '''
    return {'value': '',
            'timestamp': vc_get_timestamp(),
            'cp': vc_get_empty_vc(part, pid)}


# Verifies validity of a key.
# Compares key with the regex and key length.
def put_verify_key(key):
    '''
    input: string
    output: return True if valid
            return False if invalid
    '''
    key_regex = re.compile('[a-zA-Z0-9_]+$')
    if key_regex.match(key) and len(key) >= 1 and len(key) <= 200:
        return True
    return False


if __name__ == '__main__':
    print('Use this as an import library boy.')
    exit()
