from hash_ring import HashRing
import string
import random


def generate_random_keys(n):
    alphabet = string.ascii_lowercase
    keys = []
    for i in range(n):
        key = ''
        for _ in range(10):
            key += alphabet[random.randint(0, len(alphabet) - 1)]
        keys.append(key)
    return keys


h = HashRing(500)
for i in range(50):
    h.add_partition(i)

rand_keys = generate_random_keys(10000)

dist = {}
for part_id in h.get_partition_ids():
    dist[part_id] = 0

for key in rand_keys:
    dist[h.get_partition(key)] += 1

print(dist)
