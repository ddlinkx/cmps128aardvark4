# Tests HashRing

from hash_ring import HashRing
from pprint import PrettyPrinter

pp = PrettyPrinter()

hr = HashRing(5)

print("ADD PARTITIONS IDS 1, 2, 3")
hr.add_partition(0)
hr.add_partition(1)
hr.add_partition(2)
hr.add_partition(3)
hr.add_partition(4)

print("=========================")
print("GET PARTITION FOR KEYS")
print("%s: \t %s" % ("hello", hr.get_partition("hello")))
print("%s: \t %s" % ("bye", hr.get_partition("bye")))
print("%s: \t %s" % ("foo", hr.get_partition("foo")))
print("%s: \t %s" % ("bar", hr.get_partition("bar")))
print("%s: \t %s" % ("sike", hr.get_partition("sike")))

pp.pprint(hr.partition_mapping)

print("DELETE PARTITION ID 2")
hr.del_partition(2)
print("=========================")
print("GET PARTITION FOR KEYS")
print("%s: \t %s" % ("hello", hr.get_partition("hello")))
print("%s: \t %s" % ("bye", hr.get_partition("bye")))
print("%s: \t %s" % ("foo", hr.get_partition("foo")))
print("%s: \t %s" % ("bar", hr.get_partition("bar")))
print("%s: \t %s" % ("sike", hr.get_partition("sike")))

pp.pprint(hr.partition_mapping)
