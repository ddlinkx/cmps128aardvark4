#!flask/bin/python
from flask import Flask
from flask_restful import Resource, Api


app = Flask(__name__)
api = Api(app)

kv = {}


class GET(Resource):
    def get(self):
        return {'hello': 'world'}


api.add_resource(GET, '/')

if __name__ == '__main__':
    api.run(port=8080, debug=True)
