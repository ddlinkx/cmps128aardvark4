#!flask/bin/python
# CMPS128 - cmps129aardvark4
# {chhsiao, autbrown, tebding}@ucsc.edu
# Environment Helper Functions

from json import loads


def env_add_node_to_view(view, ipport, K):
    '''
    :param view: VIEW_DATA
    :param ipport: string representing ipport of node to be added
    :param K: number of nodes required for a partition.
    :return: new node_id, num of nodes in the system
    '''
    num_nodes_alive = env_get_node_count(view)
    # By adding a new node, a new partition is created
    if ((num_nodes_alive + 1) % K == 0):
        is_rep = True
    else:
        is_rep = False
    for node_id in view:
        if view[node_id]['IPPORT'] == ipport:
            view[node_id]['IS_REP'] = is_rep
            view[node_id]['ALIVE'] = True
            return node_id, env_get_node_count(view), False, is_rep
    new_node_id = max(view.keys()) + 1
    view[new_node_id] = {'IPPORT': ipport,
                         'IS_REP': is_rep,
                         'ALIVE': True}
    return new_node_id, env_get_node_count(view), True, is_rep


def env_sys_add_node(view, ipport, K, new_node_id):
    '''
    :param view: VIEW_DATA
    :param ipport: string representing IPPORT
    :param K: number of replicas the system should have if possible
    :param new_node_id: the node_id the node should have
    :return: new nodes node_id and num of nodes in the system
    '''
    # NOTE: Assumes no node_id conflicts
    num_reps_alive = len(env_get_rep_ipport_list(view))
    if num_reps_alive < K:
        is_rep = True
    else:
        is_rep = False
    for node_id in view:
        if view[node_id]['IPPORT'] == ipport:
            view[node_id]['IS_REP'] = is_rep
            view[node_id]['ALIVE'] = True
            return node_id, env_get_node_count(view)
    view[new_node_id] = {'IPPORT': ipport,
                         'IS_REP': is_rep,
                         'ALIVE': True}
    return new_node_id, env_get_node_count(view)


def env_get_proxy_ids(view):
    '''
    :param view: VIEW_DATA
    :return: node_ids
    '''
    node_id_lst = []
    for node_id in view:
        if not view[node_id]['IS_REP'] and view[node_id]['ALIVE']:
            node_id_lst.append(node_id)
    return node_id_lst


def env_need_promote(view, K):
    '''
    :param view: VIEW_DATA
    :param K: K
    :return: true if a proxy needs to be promoted
    '''
    reps = len(env_get_rep_ipport_list(view))
    if reps < K:
        return True
    else:
        return False


def env_get_node_count(view):
    '''
    :param view: VIEW_DATA
    :return: count of living nodes, proxies and replicas
    '''
    count = 0
    for node_id in view:
        if view[node_id]['ALIVE']:
            count += 1
    return count


# Returns a list mapping of node_id to
# IPPORT if the node is a replica and is alive.
def env_get_rep_ipport_list(view):
    '''
    input: {node_id: {IPPORT: 'IPPORT',
            IS_REP: True/False,
            'ALIVE: True'}, ...}
    output: ["IPPORT1", "IPPORT2", ...]
    '''
    rep = []
    for node_id in view:
        if view[node_id]['IS_REP'] and\
          view[node_id]['ALIVE']:
            rep.append(view[node_id]['IPPORT'])
    return rep

def env_get_prox_ipport_list(view):
    prox = []
    for node_id in view:
        if view[node_id]['IS_REP'] == False and view[node_id]['ALIVE']:
            prox.append(view[node_id]['IPPORT'])
    return prox


# Parses the VIEW environment variable and returns a dict
# mapping of node_id to IPPORT and IS_REP, a dict mapping
# of partition_id to node_id and ipport, along with
# the node_id of the local node instance.
def env_parse_view(raw_view_string, K, IPPORT):
    '''
    input: 'IPPORT, IPPORT, ...'
    output {node_id: {IPPORT: 'IPPORT',
            IS_REP: True/False,
            'ALIVE: True'}, ...},
           {part_id: [node_id,...], ...}
           node_id, part_id}
    '''
    view_dict = {}
    view_list = raw_view_string.split(',')
    num_nodes = len(view_list)
    num_prox = num_nodes % K
    num_rep = num_nodes - num_prox
    num_part = (num_nodes - num_prox) // K
    part_dict = {idx:{} for idx in range(num_part)}
    for ip_idx in range(len(view_list)):
        if view_list[ip_idx] == IPPORT:
            node_id = ip_idx + 1
            if ip_idx + 1 > num_rep:
                part_id = -1
            else:
                part_id = (ip_idx // K)
        if ip_idx + 1 <= num_rep:
            view_dict[ip_idx+1] = {'IPPORT': view_list[ip_idx],
                                'IS_REP': True,
                                'ALIVE': True}
            part_dict[ip_idx // K][ip_idx+1] = (view_list[ip_idx])
        else:
            view_dict[ip_idx+1] = {'IPPORT': view_list[ip_idx],
                                'IS_REP': False,
                                'ALIVE': True}
    return view_dict, part_dict, node_id, part_id


def env_parse_raw_view(raw_view):
    return {int(node_id): value for node_id, value
            in loads(raw_view).items()}


if __name__ == '__main__':
    print('Use this as an import library boy.')
    exit()
