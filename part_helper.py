#!flask/bin/python
# CMPS128 - cmps129aardvark4
# {chhsiao, autbrown, tebding}@ucsc.edu
# Partition Helper Functions

from json import loads
from random import randrange
import random


def part_loc_id(part, LOC_NODE_ID):
    for part_id in part:
        if LOC_NODE_ID in part[part_id]:
            return part_id
    return -1


def part_get_id_list(part):
    return [part_num for part_num in part]


def part_get_ip_list(part, partition_id):
    '''
    :param part: PART_DATA
    :param partition_id: partition_id
    :return: list of ipports
    '''
    return [ip for ip in part[partition_id].values()]


def part_get_forward_ipport(part, view, partition_id):
    possible_ips = {}
    for node_id in view:
        if view[node_id]['ALIVE']:
            if node_id in part[partition_id]:
                possible_ips[node_id] = part[partition_id][node_id]
    if possible_ips:
    	return random.choice(list(possible_ips.values()))
    else:
    	return '10.0.0.0:8080'
    # return part_dict[randrange(0, len(part_dict))]


def part_get_partition_count(part):
    '''
    :param part: PART_DATA
    :return: number of partitions
    '''
    return len(part)

# This function is only called when there exists enough
# proxies to create a new partition, which is found in env_add_node_to_view
# Any alive proxy will be included in the formation of the new partition
# NOTE:
def part_add_new_node(part, view, new_node_id):
    '''
    Adds a node to the PART_DATA dictionary passed by reference.
    Accurately handles for setting partition_ids, partitions,
    liveness and type flags.

    :param part: PART_DATA
    :param view: VIEW_DATA
    :param new_node_id: node_id to be added
    :return: nothing, modifies PART_DATA since passed by reference.
    '''
    new_part_id = max(part.keys()) + 1
    part[new_part_id] = {}
    for node_id in view:
        if view[node_id]['ALIVE']:
            if node_id == new_node_id:
                part[new_part_id][new_node_id] = view[new_node_id]['IPPORT']
            if not(view[node_id]['IS_REP']):
                view[node_id]['IS_REP'] = True
                part[new_part_id][node_id] = view[node_id]['IPPORT']
    return new_part_id


def part_promote_node(part, part_id, view, promo_node_id, old_node_id):
    '''
    Promote a node to a partition in the local PART_DATA
    '''
    del part[part_id][old_node_id]
    part[part_id][promo_node_id] = view[promo_node_id]['IPPORT']


def part_del_part(part, view, del_node_id):
    # need to redistribute keys!
    # delete part_id from partition and
    # send bulk updates to other partitions
    # returns -1 to be set into LOC_PART_ID
    part_id = part_find_node_part(part, del_node_id)
    part_members = part[part_id]
    for node_id in part_members:
        view[node_id]['IS_REP'] = False
    del part[part_id]
    return -1


def part_parse_raw_part(raw_part):
    '''
    :param raw_part: serialized PART_DATA
    :return: deserialized dictionary
    {k: {kk: int(vv) for kk, vv in v.items()} for k, v in series.items()}
    '''
    return {int(part_id): {int(node_id): ipport for node_id, ipport in node_entry.items()} for part_id, node_entry in loads(raw_part).items()}


def part_sync_hashring(hashring, part_id_list):
    '''
    Sets a hashring cache for every part_id.

    :param hashring: HashRing object
    :param part_id_list: list of part_ids
    :return: nothing
    '''
    part_id_set = set(part_id_list)
    hr_id_set = hashring.get_partition_ids()

    for part_id in hr_id_set.difference(part_id_set):
        hashring.del_partition(part_id)

    for part_id in part_id_set.difference(hr_id_set):
        hashring.add_partition(part_id)


def part_find_node_part(part, node_id):
    for part_id in part:
        if node_id in part[part_id].keys():
            return part_id
    return -1


if __name__ == '__main__':
    print('Use this as an import library boy.')
    exit()
