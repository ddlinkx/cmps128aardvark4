#!flask/bin/python
# CMPS128 - cmps129aardvark4
# {chhsiao, autbrown}@ucsc.edu
# The Key Value Store
from flask import Flask, jsonify, request
from vc_helper import vc_compare, vc_serialize, vc_get_empty_vc
from vc_helper import vc_parse_raw, vc_get_timestamp, vc_compare_timestamp
from vc_helper import vc_promotion_kv_swap, vc_get_max_vc
from env_helper import env_parse_view, env_get_rep_ipport_list
from env_helper import env_get_proxy_ids, env_get_node_count, env_need_promote
from env_helper import env_add_node_to_view, env_parse_raw_view
from put_helper import put_verify_key, put_get_empty_kv, put_get_forward_ipport
from part_helper import part_get_id_list, part_get_ip_list, part_add_new_node
from part_helper import part_parse_raw_part, part_sync_hashring, part_del_part
from part_helper import part_find_node_part, part_promote_node, part_get_forward_ipport
from part_helper import part_loc_id
from copy import deepcopy
from hash_ring import HashRing
from json import loads
import sys
import os
import requests as req_forward
from itertools import izip as zip


# Initialize Flask Application
app = Flask(__name__)

# Key structure: {key: {'value': val, 'timestamp': int}}
kv_data = {}


# PUT VALUE COMMAND
@app.route('/kv-store/<string:key>', methods=['PUT'])
def put_kv(key):
    # NOTE: If proxy, forward the req to the correct partition!
    # NOTE: If not your partition, forward the req to the correct partition.
    heartbeat()
    key_pid = PART_HR.get_partition(key)
    if not VIEW_DATA[LOC_NODE_ID]['IS_REP'] or LOC_NODE_ID not in PART_DATA[key_pid]:
        forward_ipport = part_get_forward_ipport(PART_DATA, VIEW_DATA, key_pid)
        try:
            r = req_forward.put('http://' +
                                forward_ipport +
                                '/kv-store/' + key,
                                data=request.form, timeout=5)
            return jsonify(r.json()), r.status_code
        except:
            return jsonify({'result': 'error',
                            'part_forward_ipport': forward_ipport,
                            'msg': 'Server unavailable'}), 500
    # 1. Verify Key/Value
    if put_verify_key(key):
        try:
            value = request.form['val']
        except:
            value = ''
            pass
        # Verifies existence of a value.
        if len(value) == 0:
            return jsonify({'result': 'error',
                            'msg': 'No value provided'}), 403
        if sys.getsizeof(value) > 1024:
            return jsonify({'result': 'error',
                            'msg': 'Object too large. Size limit is 1MB'}), 403
        # 2. Compare Replica VC against the request's causal_payload
        # key_exists = key in kv_data
        rep_part_list = part_get_ip_list(PART_DATA, key_pid)
        for ipport in rep_part_list:
            if ipport != IPPORT:
                try:
                    r = req_forward.put('http://' + ipport +
                                        '/kv-store/validated_put/'
                                        + key + "?sender=" +
                                        str(LOC_NODE_ID), data=request.form,
                                        timeout=0.5)
                except:
                    continue
            else:
                continue
        temp_timestamp = vc_get_timestamp()
        if key not in kv_data:
            kv_data[key] = put_get_empty_kv(PART_DATA, key_pid)
        local_vc = kv_data[key]['cp']
        if request.form['causal_payload'] == '' or request.form['causal_payload'] == '\'\'':
            request_vc = vc_get_empty_vc(PART_DATA, key_pid)
        elif request.form['causal_payload'] == '9.9.9.9':
            request_vc = vc_get_max_vc(PART_DATA, key_pid)
        else:
            request_vc = vc_parse_raw(request.form['causal_payload'])
        # return jsonify({'request cp':request.form['causal_payload']})
        valid_req = vc_compare(local_vc, request_vc)  # T/F/None
        used_timestamp = False
        if valid_req is None:  # resolve concurrent vc using timestamp
            # if temp_timestamp < kv_data[key][timestamp], then invalid
            used_timestamp = True
            valid_req = vc_compare_timestamp(kv_data[key]['timestamp'],
                                             temp_timestamp)
        if valid_req:
            # Inserts kv pair and timestamp AND increment clock.
            # NOTE: Handles for prior key existence.
            # increment local nodes key vc position value
            # TODO: make sure local_vc is incremented after the following line
            kv_data[key]['value'] = value
            kv_data[key]['timestamp'] = temp_timestamp
            kv_data[key]['cp'] = request_vc
            kv_data[key]['cp'][LOC_NODE_ID] += 1
            # TODO: Remove this return to test validated broadcast

            return jsonify({'result': 'success',
                            'node_id': LOC_NODE_ID,
                            'partition_id': LOC_PART_ID,
                            'causal_payload': vc_serialize(kv_data[key]['cp']),
                            'cp_2': kv_data[key]['cp'],
                            'timestamp': kv_data[key]['timestamp']}), 200
        else:
            return jsonify({'result': 'error',
                            'msg': 'Invalid VC, GET and try again.',
                            'used_ts': used_timestamp}), 403
    # If key is not valid, return 'Key not valid' error.
    else:
        return jsonify({'result': 'error', 'msg': 'Key not valid'}), 403


# Endpoint to broadcast key to other replica's in the partition
@app.route('/kv-store/validated_put/<string:key>', methods=['PUT'])
def put_kv_from_replica(key):
    value = request.form['val']
    sender_node_id = int(request.args['sender'])
    key_pid = part_loc_id(PART_DATA, sender_node_id)
    temp_timestamp = vc_get_timestamp()
    if key not in kv_data:
        kv_data[key] = put_get_empty_kv(PART_DATA, key_pid)
    if request.form['causal_payload'] == '':
        request_vc = vc_get_empty_vc(PART_DATA, key_pid)
    else:
        request_vc = vc_parse_raw(request.form['causal_payload'])

    local_vc = kv_data[key]['cp']
    valid_req = vc_compare(local_vc, request_vc)  # T/F/None
    if valid_req is None:  # resolve concurrent vc using timestamp
        # if temp_timestamp < kv_data[key][timestamp], then invalid
        valid_req = vc_compare_timestamp(kv_data[key]['timestamp'],
                                         temp_timestamp)
    if valid_req:
        # Inserts kv pair and timestamp AND increment clock.
        # NOTE: Handles for prior key existence.
        # increment local nodes key vc position value
        # TODO: make sure local_vc is incremented after the following line
        kv_data[key]['cp'] = request_vc
        kv_data[key]['cp'][sender_node_id] += 1
        kv_data[key]['value'] = value
        kv_data[key]['timestamp'] = temp_timestamp
        return jsonify({'result': 'success',
                        'kv': kv_data[key]}), 200
    else:
        return jsonify({'result': 'error',
                        'msg': 'Invalid VC, GET and try again.',
                        'node_kv': kv_data[key],
                        'req_cp': request_vc}), 403


@app.route('/kv-store/<string:key>', methods=['GET'])
def get_kv_read_repair(key):
    '''
    :params: key
    :returns: {'result': 'Success',
             'value': value,
             'node_id': local node id
             'causal_payload': cp comp format,
             'timestamp': timestamp of last write}, 200
        or error 404
    '''
    heartbeat()
    key_pid = PART_HR.get_partition(key)
    if not VIEW_DATA[LOC_NODE_ID]['IS_REP'] or LOC_NODE_ID not in PART_DATA[key_pid]:
        forward_ipport = part_get_forward_ipport(PART_DATA, VIEW_DATA, key_pid)
        try:
            r = req_forward.get('http://' +
                                forward_ipport +
                                '/kv-store/' + key,
                                data=request.form, timeout=5)
            return jsonify(r.json()), r.status_code
        except:
            return jsonify({'result': 'error',
                            'VIEW_DATA': VIEW_DATA,
                            'part_forward_ipport': forward_ipport,
                            'msg': 'Server unavailable',
                            'key_pid': str(key_pid)}), 500
    if key not in kv_data:
        max_kv = {'value': '',
                  'timestamp': 0,
                  'cp': vc_get_empty_vc(PART_DATA, key_pid)}
    else:
        max_kv = kv_data[key]
    # Iterate through every replica to find key with higher vc.
    all_rep_resp = {}
    for rep_ipport in part_get_ip_list(PART_DATA, key_pid):
        if rep_ipport == IPPORT:
            continue
        try:
            r = req_forward.get('http://' + rep_ipport
                            + '/kv-store/verify_val/' + key, timeout=5)
        except:
            continue
        rep_resp = r.json()
        # Replica does not know the key.
        if rep_resp['result'] == 'error':
            continue
        all_rep_resp[rep_ipport] = rep_resp
        if rep_resp['causal_payload'] == '':
            replica_vc = vc_get_empty_vc(PART_DATA, key_pid)
        else:
            replica_vc = vc_parse_raw(rep_resp['causal_payload'])
        replica_ts = rep_resp['timestamp']
        vc_comp_result = vc_compare(max_kv['cp'], replica_vc)
        if vc_comp_result is None:    # VCs are concurrent
            if vc_compare_timestamp(max_kv['timestamp'], replica_ts):
                max_kv = {'value': rep_resp['value'],
                          'timestamp': rep_resp['timestamp'],
                          'cp': vc_parse_raw(rep_resp['causal_payload'])}
        if vc_comp_result:            # replica VC has larger VC
            max_kv = {'value': rep_resp['value'],
                      'timestamp': rep_resp['timestamp'],
                      'cp': vc_parse_raw(rep_resp['causal_payload'])}
    # Read Repair returns most recent kv_data
    if max_kv['timestamp'] == 0:
        return jsonify({'result': 'error',
                        'msg': 'Key does not exist'}), 404
    else:
        kv_data[key] = max_kv
        return jsonify({'result': 'success',
                        'value': kv_data[key]['value'],
                        'node_id': LOC_NODE_ID,
                        'partition_id': key_pid,
                        'causal_payload': vc_serialize(kv_data[key]['cp']),
                        'timestamp': kv_data[key]['timestamp'],
                        'all_reps': all_rep_resp}), 200


# Simply returns the key value without read repair
@app.route('/kv-store/verify_val/<string:key>', methods=['GET'])
def get_kv_stupid(key):
    '''
    input: key
    output: {'result': 'Success',
             'value': value,
             'node_id': local node id,
             'causal_payload': cp comp format,
             'timestamp': timestamp of last write}, 200
        or error 404
    '''
    if key in kv_data:
        return jsonify({'result': 'success',
                        'value': kv_data[key]['value'],
                        'node_id': LOC_NODE_ID,
                        'causal_payload': vc_serialize(kv_data[key]['cp']),
                        'timestamp': kv_data[key]['timestamp']}), 200
    else:
        return jsonify({'result': 'error',
                        'msg': 'Key does not exist'}), 404


# GET NODE DETAILS
@app.route('/kv-store/get_node_details', methods=['GET'])
def get_node_details():
    '''
    input: None
    output: returns whether the node is a replica
    '''
    heartbeat()
    if VIEW_DATA[LOC_NODE_ID]['IS_REP']:
        is_replica = 'Yes'
    else:
        is_replica = 'No'
    return jsonify({'result': 'Success',
                   'replica': is_replica}), 200


# GET ALL REPLICAS
@app.route('/kv-store/get_all_replicas', methods=['GET'])
def get_all_replicas():
    '''
    input: None
    output: returns list of replicas
    '''
    heartbeat()
    replicas = env_get_rep_ipport_list(VIEW_DATA)
    return jsonify({'result': 'success',
                    'replicas': replicas}), 200


# GET PARTITION ID
@app.route('/kv-store/get_partition_id', methods=['GET'])
def get_partition_id():
    heartbeat()
    try:
        return jsonify({'result': 'success',
                        'partition_id': LOC_PART_ID}), 200
    except:
        return jsonify({'result': 'error',
                        'error': 'get_partition_id has failed'}), 403

# GET ALL PARTITION IDS
@app.route('/kv-store/get_all_partition_ids', methods=['GET'])
def get_all_partition_ids():
    heartbeat()
    try:
        partition_id_list = part_get_id_list(PART_DATA)
        return jsonify({'result': 'success',
                        'partition_id_list': partition_id_list,
                        'VIEW_DATA': VIEW_DATA,
                        'partition_mapping': PART_DATA}), 200
    except:
        return jsonify({'result': 'error',
                        'error': 'get_all_partition_ids has failed'}), 403

# GET PARTITION MEMBERS
@app.route('/kv-store/get_partition_members', methods=['GET'])
def get_partition_members():
    try:
        partition_id = int(request.form['partition_id'])
        members_list = part_get_ip_list(PART_DATA, partition_id)
        return jsonify({'result': 'success',
                        'partition_members': members_list}), 200
    except:
        return jsonify({'result': 'error',
                        'error': 'get_partition_members has failed'}), 403

@app.route('/test', methods=['GET'])
def get_test_vals():
    return jsonify({'IP': EXTERNAL_IP,
                    'PORT': PORT,
                    'IPPORT': IPPORT,
                    'node_id': LOC_NODE_ID})


@app.route('/sys/heartbeat', methods=['GET'])
def send_heartbeat():
    return jsonify({'result':'success'}), 200


# Broadcasts a heartbeat listening for a response from all nodes in the system.
def heartbeat():
    global LOC_NODE_ID
    for node_id in VIEW_DATA:
        if node_id != LOC_NODE_ID:
            ipport = VIEW_DATA[node_id]['IPPORT']
            try:
                r = req_forward.get('http://' + ipport + '/sys/heartbeat', timeout=0.2)
                r.json()
                if VIEW_DATA[node_id]['ALIVE']:  # cool, he's still alive nbd
                    continue
                else:  # Revive our guy using env_add_node_to_view
                    env_add_node_to_view(VIEW_DATA, ipport, K)
            except:  # damn they dead
                if VIEW_DATA[node_id]['ALIVE'] == True:
                    VIEW_DATA[node_id]['ALIVE'] = False


@app.route('/kv-store/update_view', methods=['PUT'])
def update_view():
    #heartbeat()
    global LOC_NODE_ID
    update_type = request.args['type']  # can be add or remove
    ipport = request.form['ip_port']
    if update_type == 'add':
        # Update local VIEW
        new_node_id, view_node_count, actually_new_node, new_partition =\
            env_add_node_to_view(VIEW_DATA, ipport, K)

        # New partition is only true if there exists enough nodes for new partition
        remapped_kv = {}
        new_rep_cant_redistribute = True
        if new_partition:
            # Reserve our local node id's for mapping if possible
            if part_loc_id(PART_DATA, LOC_NODE_ID) != -1:
                local_pid = part_loc_id(PART_DATA, LOC_NODE_ID)
                prev_id_list = list(PART_DATA[local_pid].keys())
                prev_id_list.sort()
                new_rep_cant_redistribute = False
            else:
                new_rep_cant_redistribute = True

            # Add the new partition to our view + modify hashring
            part_id = part_add_new_node(PART_DATA, VIEW_DATA, new_node_id)
            PART_HR.add_partition(part_id)
            part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))

            # KV Mapping + Partition redistribution
            if new_rep_cant_redistribute == False:
                part_id_set = PART_HR.get_partition_ids()
                for part_id in part_id_set:
                    remapped_kv[part_id] = {}
                for key in kv_data:
                    key_pid = PART_HR.get_partition(key)
                    remapped_kv[key_pid][key] = kv_data[key]
                for part_id in part_id_set:
                    node_id_list = list(PART_DATA[part_id].keys())
                    node_id_list.sort()
                    node_id_mapping = dict(zip(prev_id_list, node_id_list))
                    for key in remapped_kv[part_id]:
                        for our_nid, new_nid in node_id_mapping.items():
                            remapped_kv[part_id][key]['cp'][new_nid] = remapped_kv[part_id][key]['cp'].pop(our_nid)
        # Catch-up the new nodes VIEW_DATA + PART_DATA
        if actually_new_node:
            view_snapshot = {'your_node_id': new_node_id,
                        'view': vc_serialize(VIEW_DATA),
                        'K': K}
            part_snapshot = {'part': vc_serialize(PART_DATA)}
            try:
                r = req_forward.put('http://' + VIEW_DATA[new_node_id]['IPPORT'] +
                            '/sys/view_catchup', data=view_snapshot)
            except:
                return jsonify({'result': 'failure to catchup new node'}), 403
            try:
                r = req_forward.put('http://' + VIEW_DATA[new_node_id]['IPPORT'] +
                            '/sys/part_catchup', data=part_snapshot)
            except:
                return jsonify({'result': 'failure to catchup new node'}), 403
        # Broadcast View Change to all available nodes
        for node_id in VIEW_DATA:
            if (LOC_NODE_ID != node_id) and (VIEW_DATA[node_id]['IPPORT'] != ipport):
                try:
                    req_forward.put('http://' + VIEW_DATA[node_id]['IPPORT'] +
                                '/sys/add_node', data=request.form, timeout=2)
                except:
                    continue
        # Redistribute KVs to new partitions if necessary
        if new_partition and new_rep_cant_redistribute == False:
            # First broadcast keys to their new partition
            for part_id in PART_DATA:
                if part_id == part_loc_id(PART_DATA, LOC_NODE_ID):
                    continue
                iplist = part_get_ip_list(PART_DATA, part_id)
                for ipport in iplist:
                    try:
                        r = req_forward.put('http://' + ipport + '/sys/distributed_kvs',
                                            data={'raw':vc_serialize(remapped_kv[part_id])})
                    except:
                        continue
            # dab on the haters
            # Then remove keys from our own kv_data
            for part_id in PART_DATA:
                if part_id == part_loc_id(PART_DATA, LOC_NODE_ID):
                    continue
                for key in remapped_kv[part_id]:
                    kv_data.pop(key)
        return jsonify({'result': 'success',
                        'node_id': new_node_id,
                        'number_of_nodes': view_node_count,
                        'view_data': VIEW_DATA,
                        'part_data': PART_DATA,
                        'kv_data': remapped_kv,
                        'partition_id': part_loc_id(PART_DATA, LOC_NODE_ID),
                        'number_of_partitions': len(PART_DATA)}), 200
    else:
        all_reps = {}
        for node_id in VIEW_DATA:
            # Find node_id to delete
            if VIEW_DATA[node_id]['IPPORT'] == ipport:
                VIEW_DATA[node_id]['ALIVE'] = False
                # IF the node is a replica, we must try PROMOTION or DYING BREATH
                # ELSE node is a proxy
                if VIEW_DATA[node_id]['IS_REP']:
                    VIEW_DATA[node_id]['IS_REP'] = False
                    # Either PROMOTION or DYING BREATH
                    # Find the node's partition
                    remove_part_id = part_find_node_part(PART_DATA, node_id)
                    proxy_id_lst = env_get_proxy_ids(VIEW_DATA)
                    # DYING BREATH
                    # no proxies means everyone is a replica in a partition
                    if len(proxy_id_lst) == 0:
                        # Need to save our partitions list of node_ids for remapping
                        prev_id_list = list(PART_DATA[remove_part_id].keys())
                        prev_id_list.sort()

                        # Delete node from local partition, set nodes[IS_REP] to false
                        curr_part_id = part_find_node_part(PART_DATA, LOC_NODE_ID)
                        part_del_part(PART_DATA, VIEW_DATA, node_id)
                        PART_HR.del_partition(remove_part_id)
                        # Our local hashring now only contains left over partitions
                        part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))

                        # Is the node in our partition? We do the work of distributing keys
                        if curr_part_id == remove_part_id:
                            remapped_kv = {}
                            part_id_set = PART_HR.get_partition_ids()
                            for part_id in part_id_set:
                                remapped_kv[part_id] = {}
                            for key in kv_data:
                                key_pid = PART_HR.get_partition(key)
                                remapped_kv[key_pid][key] = kv_data[key]

                            for part_id in part_id_set:
                                iplist = part_get_ip_list(PART_DATA, part_id)
                                node_id_list = list(PART_DATA[part_id].keys())
                                node_id_list.sort()
                                node_id_mapping = dict(zip(prev_id_list, node_id_list))
                                for key in remapped_kv[part_id]:
                                    for our_nid, new_nid in node_id_mapping.items():
                                        remapped_kv[part_id][key]['cp'][new_nid] = remapped_kv[part_id][key]['cp'].pop(our_nid)
                                for ipport in iplist:
                                    try:
                                        r = req_forward.put('http://' + ipport + '/sys/distributed_kvs',
                                                        data={'raw':vc_serialize(remapped_kv[part_id])})
                                    except:
                                        continue
                        del VIEW_DATA[node_id]
                        for nid in VIEW_DATA:
                            if nid != LOC_NODE_ID:
                                ipport = VIEW_DATA[nid]['IPPORT']
                                try:
                                    r = req_forward.put('http://' + ipport +
                                                    '/sys/del_node', data={'node_id':node_id})
                                except:
                                    continue
                                all_reps[ipport] = r.json()
                        break
                    # PROMOTION
                    else:
                        # Promote the proxy with the smallest node_id
                        promoted_id = min(proxy_id_lst)
                        VIEW_DATA[promoted_id]['IS_REP'] = True
                        part_promote_node(PART_DATA, remove_part_id, VIEW_DATA, promoted_id, node_id)
                        curr_part_id = part_find_node_part(PART_DATA, promoted_id)

                        # If the promoted node is in my partition, I send him my kv_data
                        if remove_part_id == curr_part_id:
                            vc_promotion_kv_swap(kv_data, node_id, promoted_id)
                            try:
                                r = req_forward.put('http://' + VIEW_DATA[promoted_id]['IPPORT'] +
                                                '/sys/promotion_kvs', data={'raw': vc_serialize(kv_data)})
                            except:
                                continue
                        del VIEW_DATA[node_id]
                        for nid in VIEW_DATA:
                            if nid != LOC_NODE_ID:
                                try:
                                    r = req_forward.put('http://' + VIEW_DATA[nid]['IPPORT'] +
                                                    '/sys/del_node', data={'node_id':node_id})
                                except:
                                    continue
                        break
                else:
                    del VIEW_DATA[node_id]
                    for nid in VIEW_DATA:
                        if nid != LOC_NODE_ID:
                            try:
                                r = req_forward.put('http://' + VIEW_DATA[nid]['IPPORT'] + 
                                                '/sys/del_node', data={'node_id':node_id})
                            except:
                                continue
                    break
        return jsonify({'result': 'success',
                        'number_of_nodes': env_get_node_count(VIEW_DATA),
                        'view_data': VIEW_DATA,
                        'part_data': PART_DATA,
                        'all_reps': all_reps,
                        'number_of_partitions': len(PART_DATA)}), 200


@app.route('/sys/add_node', methods=['PUT'])
def sys_add_node():
    try:
        ipport = request.form['ip_port']
        new_node_id, view_node_count, actually_new, new_partition =\
                env_add_node_to_view(VIEW_DATA, ipport, K)
        if new_partition:
            remapped_kv = {}
            new_rep_cant_redistribute = True

            # Reserve our local node id's for mapping!
            if part_loc_id(PART_DATA, LOC_NODE_ID) != -1:
                local_pid = part_loc_id(PART_DATA, LOC_NODE_ID)
                prev_id_list = list(PART_DATA[local_pid].keys())
                prev_id_list.sort()
                new_rep_cant_redistribute = False
            else:
                new_rep_cant_redistribute = True

            # Add the new partition to our view + modify hashring
            part_id = part_add_new_node(PART_DATA, VIEW_DATA, new_node_id)
            PART_HR.add_partition(part_id)
            part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))

            if new_rep_cant_redistribute == False:
                # KV Mapping + Partition redistribution
                part_id_set = PART_HR.get_partition_ids()
                for part_id in part_id_set:
                    remapped_kv[part_id] = {}
                for key in kv_data:
                    key_pid = PART_HR.get_partition(key)
                    remapped_kv[key_pid][key] = kv_data[key]
                for part_id in part_id_set:
                    node_id_list = list(PART_DATA[part_id].keys())
                    node_id_list.sort()
                    node_id_mapping = dict(zip(prev_id_list, node_id_list))
                    for key in remapped_kv[part_id]:
                        for our_nid, new_nid in node_id_mapping.items():
                            remapped_kv[part_id][key]['cp'][new_nid] = remapped_kv[part_id][key]['cp'].pop(our_nid)
                # First broadcast keys to their new partition
                for part_id in PART_DATA:
                    if part_id == part_loc_id(PART_DATA, LOC_NODE_ID):
                        continue
                    iplist = part_get_ip_list(PART_DATA, part_id)
                    for ipport in iplist:
                        try:
                            r = req_forward.put('http://' + ipport + '/sys/distributed_kvs', data={'raw':vc_serialize(remapped_kv[part_id])})
                        except:
                            continue
                # Then remove keys from our own kv_data
                for part_id in PART_DATA:
                    if part_id == part_loc_id(PART_DATA, LOC_NODE_ID):
                        continue
                    for key in remapped_kv[part_id]:
                        kv_data.pop(key)
        return jsonify({'result': 'success',
                        'new_node_id': new_node_id,
                        'view_node_count': view_node_count}), 200
    except:
        return jsonify({'error': 'error'}), 200

# Endpoint for nodes after an update_view?type=remove request
@app.route('/sys/del_node', methods=['PUT'])
def sys_del_node():
    try:
        node_id = int(request.form['node_id'])
        VIEW_DATA[node_id]['ALIVE'] = False
        # IF the node is a replica, we must try PROMOTION or DYING BREATH
        # ELSE node is a proxy
        if VIEW_DATA[node_id]['IS_REP']:
            VIEW_DATA[node_id]['IS_REP'] = False
            # Either PROMOTION or DYING BREATH
            # Find the node's partition
            remove_part_id = part_find_node_part(PART_DATA, node_id)
            proxy_id_lst = env_get_proxy_ids(VIEW_DATA)
            # DYING BREATH
            if len(proxy_id_lst) == 0:
                prev_id_list = list(PART_DATA[remove_part_id].keys())
                prev_id_list.sort()
                # Delete node from local partition, set nodes[IS_REP] to false
                curr_part_id = part_find_node_part(PART_DATA, LOC_NODE_ID)
                part_del_part(PART_DATA, VIEW_DATA, node_id)
                PART_HR.del_partition(remove_part_id)
                # Our local hashring now only contains left over partitions
                part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))
                # Is the node in our partition? We do the work of distributing keys
                if curr_part_id == remove_part_id:
                    remapped_kv = {}
                    part_id_set = PART_HR.get_partition_ids()
                    for part_id in part_id_set:
                        remapped_kv[part_id] = {}
                    for key in kv_data:
                        key_pid = PART_HR.get_partition(key)
                        remapped_kv[key_pid][key] = kv_data[key]
                    for part_id in part_id_set:
                        iplist = part_get_ip_list(PART_DATA, part_id)
                        node_id_list = list(PART_DATA[part_id].keys())
                        node_id_list.sort()
                        node_id_mapping = dict(zip(prev_id_list, node_id_list))
                        for key in remapped_kv[part_id]:
                            for our_nid, new_nid in node_id_mapping.items():
                                remapped_kv[part_id][key]['cp'][new_nid] = remapped_kv[part_id][key]['cp'].pop(our_nid)
                        for ipport in iplist:
                            try:
                                r = req_forward.put('http://' + ipport + '/sys/distributed_kvs',
                                                    data={'raw':vc_serialize(remapped_kv[part_id])})
                            except:
                                continue
            # PROMOTION
            else:
                # Promote the proxy with the smallest node_id
                promoted_id = min(proxy_id_lst)
                VIEW_DATA[promoted_id]['IS_REP'] = True
                part_promote_node(PART_DATA, remove_part_id, VIEW_DATA, promoted_id, node_id)
                curr_part_id = part_find_node_part(PART_DATA, promoted_id)

                # If the promoted node is in my partition, I send him my kv_data
                if part_find_node_part(PART_DATA, LOC_NODE_ID) == curr_part_id:
                    vc_promotion_kv_swap(kv_data, node_id, promoted_id)
                    r = req_forward.put('http://' + VIEW_DATA[promoted_id]['IPPORT'] + '/sys/promotion_kvs', data={'raw': vc_serialize(kv_data)})
            del VIEW_DATA[node_id]
        return jsonify({'result': 'success'}), 200
    except:
        return jsonify({'result': '/sys/del_node/ has failed'}), 403


# This endpoint is ONLY queried if you're a newly promoted node
# Take the kv_data sent from a node in your new partition
@app.route('/sys/promotion_kvs', methods=['PUT'])
def sys_take_kvs():
    try:
        kv_entry = loads(request.form['raw'])
        for key in kv_entry:
            kv_entry[key]['cp'] = {int(node_id): int(clock_val) for node_id, clock_val in kv_entry[key]['cp'].items()}
        kv_data.update(kv_entry)
        return jsonify({'result': 'success'}), 200
    except:
        return jsonify({'error': 'error /sys/promotion_kvs'}), 403


# Whenever this endpoint is queried, a node is sending you your newly remapped kv_data!
# This only occurs when there is a removal of a partition.
@app.route('/sys/distributed_kvs', methods=['PUT'])
def sys_accept_kvs():
    try:
        kv_merge = loads(request.form['raw'])
        for key in kv_merge:
            kv_merge[key]['cp'] = {int(node_id): int(clock_val) for node_id, clock_val in kv_merge[key]['cp'].items()}
        kv_data.update(kv_merge)
        return jsonify({'result': 'success'}), 200
    except:
        return jsonify({'error': 'could not store new kv_data'}), 403


# 1.) Whoever sends this must send you their view
# 2.) You adopt their view completely.
@app.route('/sys/view_catchup', methods=['PUT'])
def sys_view_catchup():
    '''
    your_node_id: int,
    view: senders VIEW_DATA
    :return:
    '''
    global VIEW_DATA
    global K
    global LOC_NODE_ID

    senders_view = env_parse_raw_view(request.form['view'])
    LOC_NODE_ID = deepcopy(int(request.form['your_node_id']))
    K = deepcopy(int(request.form['K']))
    VIEW_DATA = deepcopy(senders_view)
    return jsonify({'result': 'success', 'loc_nodeid': LOC_NODE_ID,
                    'view': str(VIEW_DATA), 'K': K,
                    'my_entry': str(VIEW_DATA[LOC_NODE_ID]),
                    'view_addr': id(VIEW_DATA),
                    'nodeid_addr': id(LOC_NODE_ID)}), 200


@app.route('/sys/part_catchup', methods=['PUT'])
def sys_part_catchup():
    global PART_DATA
    senders_part = part_parse_raw_part(request.form['part'])
    PART_DATA = deepcopy(senders_part)
    part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))
    return jsonify({'result': 'success',
                    'part': PART_DATA}), 200

'''
@app.before_request
def sync_view():
    if request.endpoint != 'heartbeat':
        heartbeat()
'''
if __name__ == '__main__':
    # Sets the externally visible IP & PORT of this instance.
    # TODO: CURRENT STARTUP ASSUMES VIEW IS AN ENVIRONMENT VARIABLE!!!
    # TODO: It is the responsibility of the receiver of the INITIAL update_view add to send their VIEW_DATA to the new node!!!
    # If there's no VIEW, just startup and chill out.
    #
    #os.environ['IPPORT'] = ["0.0.0.0:8080"]
    #os.environ['K'] = '1'

    IPPORT = os.getenv('IPPORT')
    EXTERNAL_IP = IPPORT[:IPPORT.find(':')]
    PORT = IPPORT[IPPORT.find(':')+1:]

    PART_HR = HashRing(50)
    try:
        VIEW = os.getenv('VIEW')
        K = int(os.getenv('K'))
        VIEW_DATA, PART_DATA, LOC_NODE_ID, LOC_PART_ID = env_parse_view(VIEW, K, IPPORT)
        part_sync_hashring(PART_HR, part_get_id_list(PART_DATA))
    except:
        VIEW_DATA = {}
        PART_DATA = {}
        LOC_NODE_ID = 0
        LOC_PART_ID = -1
        K = 0
    app.run(host=EXTERNAL_IP, port=int(PORT), debug=True, use_reloader=False, threaded=True)
