#!flask/bin/python
# CMPS128 - cmps129aardvark4
# {chhsiao, autbrown, tebding}@ucsc.edu
# VC Helper Functions

from time import time
from json import loads, dumps
from random import randint


# Converts vc_dict to serializable string
# to be returned with responses.
def vc_serialize(vc_dict):
    '''
    input: vc_dict = {node_id: clock_val, ...}
    return: "{"node_id": clock_val, ...}"
    '''
    return dumps(vc_dict)


# Given raw_vc, return a dict of integers with entries
def vc_parse_raw(raw_vc):
    '''
    input: raw_vc = "{'node_id': clock_val}"
    output: {node_id: clock_val}
    '''
    return {int(node_id): int(clock_val) for node_id, clock_val
            in loads(raw_vc).items()}


# Causally compares two vector clocks.
# Returns True if a < b (a comes before b)
# Returns False if a > b (b comes before a)
# Returns None if a || b (a and b are concurrent)
# Structure of vc: {node_id: clock_val, ...}
def vc_compare(a, b):
    '''
    input: a, b = {node_id: clock_val, ...}
    output: true if a < b
            false if a > b
            None if a || b
    '''
    # TODO: Handle for missing keys
    flag = 0
    try:
        for id in a:
            if a[id] == b[id]:
                continue
            elif a[id] < b[id]:
                if flag == 0:
                    flag = 1
                else:
                    if flag != 1:
                        return None
                    else:
                        continue
            else:
                if flag == 0:
                    flag = -1
                else:
                    if flag != -1:
                        return None
                    else:
                        continue
    except:
        return None
    if flag == 1 or flag == 0:
        return True
    else:
        return False

# Generates an empty vector clock
def vc_get_empty_vc(partition, pid):
    '''
    input: {node_id: {IPPORT: 'IPPORT',
            IS_REP: True/False},
            'ALIVE': bool}, ...}
    output: {node_id: 0, ...}
    '''
    empty_vc = {}
    for node_id in partition[pid]:
        empty_vc[node_id] = 0
    return empty_vc

def vc_get_max_vc(partition, pid):
    max_vc = {}
    for node_id in partition[pid]:
        max_vc[node_id] = 999
    return max_vc


# Given raw_vc, return a dict of integers with entries
def vc_parse_raw(raw_vc):
    '''
    input: raw_vc = "{'node_id': clock_val}"
    output: {node_id: clock_val}
    '''
    return {int(node_id): int(clock_val) for node_id, clock_val
            in loads(raw_vc).items()}


# Returns the current time stamp with decimals truncated.
def vc_get_timestamp():
    '''
    input: None
    output: returns the current timestamp with decimals truncated.
    '''
    return int(time())


# Compare two timestamps to determine who came first.
# Randomly decide if timestamps are identical
def vc_compare_timestamp(ts1, ts2):
    '''
    input: ts1 and ts2 = int
    output: true if ts1 < ts2
            false if ts1 > ts2
            randomly choose if ts1 == ts2
    '''
    if ts1 < ts2:
        return True
    elif ts1 > ts2:
        return False
    else:
        winner_winner_chicken_dinner = randint(1, 2)
        if winner_winner_chicken_dinner == 1:
            return True
        else:
            return False


# Given a kv_data, an old node id (being removed) and a new node id
# Return a kv_data with the id's swapped.
def vc_promotion_kv_swap(kvd, old_node_id, new_node_id):
    '''
    input: kv_data, old node id, new node id
    output: swaps of id's in kv_data
    '''
    for key in kvd:
        old_clock_val = kvd[key]['cp'][old_node_id]
        del kvd[key]['cp'][old_node_id]
        kvd[key]['cp'][new_node_id] = old_clock_val

if __name__ == '__main__':
    print('Use this as an import library boy.')
    exit()
