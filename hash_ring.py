# Class Implementation of Consistent Hashing (Hash Ring)
# Chris Hsiao - chhsiao - 12/7/17

# Defines a HashRing object that implements consistent hashing for mapping
# keys to partitions. We will be using md5 to hash, and will implement cache
# replicas for more uniform key distribution across caches.

# Disclaimer: Initial implementation performed linear traversal of unsorted
# dictionary keys of hash values for partition_id mapping. This was super expensive
# because the keys were unsorted, and for a large number of replicas be disastrous,
# but it's something you need to do. So I found a tutorial by Mike Bayer that introduces
# using the bisect module for significantly faster ordered insertions and locating indexes.
# My initial implementation was based on an explanation by Tom White, which used Java's TreeMap,
# which does what bisect does. I then modified my implementation based on Tom White's explanation
# to use Mike Bayer's bisect with an ordered hashes array. The article can be found below:
# http://techspot.zzzeek.org/2012/07/07/the-absolutely-simplest-consistent-hashing-example/

from bisect import insort, bisect, bisect_left
from hashlib import md5


class HashRing(object):
    def __init__(self, replicas):
        '''
        Initializes an empty HashRing object with number of replicas set.
        Runtime increases with self.replicas value, but leads to better uniformity.
        Use self.hashes to store a sorted hash values.
        Use bisect module to avoid linear traversal of keys list, and do ordered inserts.

        self.replicas = number of replicas per partition_id bucket
        self.hashes = ordered list of hashes
        self.partition_mapping = mapping of hashes to partition_ids

        :param replicas: number of replicas per cache for even distribution.
        '''
        self.replicas = replicas
        self.partition_ids = []
        self.hashes = []
        self.partition_mapping = {}

    def hash(self, key):
        '''
        Hashes a key using md5.

        :param key: string representing key to be hashed
        :return: md5 hash of the key
        '''
        return md5(str(key).encode()).hexdigest()

    def add_partition(self, partition_id):
        '''
        Inserts self.replicas number of partition entries into the mapping.

        :param partition_id: partition id to be added to the mapping
        :return: 1 for success, -1 for failure
        '''
        for i in range(self.replicas):
            hash = self.hash("%s:%s" % (partition_id, i))
            if hash in self.hashes:
                return -1
            else:
                self.partition_mapping[hash] = partition_id
                insort(self.hashes, hash)
        self.partition_ids.append(partition_id)
        return 1

    def del_partition(self, partition_id):
        '''
        Deletes every partition entry from the mapping.
        Failure means the partition didn't exist because the hashes couldn't be found.

        :param partition_id: partition id to be removed from the mapping
        :return: 1 for success, -1 for failure
        '''
        for i in range(self.replicas):
            hash = self.hash("%s:%s" % (partition_id, i))
            if hash in self.hashes:
                del self.partition_mapping[hash]
                idx = bisect_left(self.hashes, hash)
                del self.hashes[idx]
            else:
                return -1
        del self.partition_ids[bisect_left(self.partition_ids, partition_id)]
        return 1

    def get_partition(self, key):
        '''
        Returns the partition_id the key maps to.

        :param key: string representing the key in a key, value pair.
        :return: string corresponding to the
        '''
        hash = self.hash(key)
        start_loc = bisect(self.hashes, hash)
        if start_loc == len(self.hashes):
            start_loc = 0
        return self.partition_mapping[self.hashes[start_loc]]

    def get_partition_ids(self):
        '''
        :return: set of all partition ids
        '''
        return set(self.partition_ids)


